import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

options = webdriver.ChromeOptions()
options.add_experimental_option("excludeSwitches", ['enable-automation']);

driver1 = webdriver.Chrome(options=options)
driver2 = webdriver.Chrome(options=options)

driver1.set_window_position(0, 0)
driver2.set_window_position(0, 1000)

driver1.get("http://www.python.org")
driver2.get("http://www.bremen.de")

driver1.fullscreen_window()
driver2.fullscreen_window()


time.sleep(5)
driver1.close()
driver2.close()
