
# Prometheus
sudo useradd --no-create-home --shell /bin/false prometheus
sudo useradd --no-create-home --shell /bin/false nodeexporter
sudo useradd --no-create-home --shell /bin/false alertmanager

## Install server
Get latest version from prometheus [here](https://github.com/prometheus/prometheus/releases)
``` bash
cd /opt
echo "curl -SL https://github.com/prometheus/prometheus/releases/download/v2.20.1/prometheus-2.20.1.linux-armv7.tar.gz >prometheus.tgz" | sudo sh
sudo tar xvzf prometheus.tgz
sudo rm prometheus.tgz
sudo ln -s prometheus-2.20.1.linux-armv7 prometheus

sudo mv /tmp/prometheusfile /etc/systemd/system/prometheus.service
sudo systemctl daemon-reload \
 && sudo systemctl enable prometheus \
 && sudo systemctl start prometheus
```

## Install clients
Get latest version from node exporter [here](https://github.com/prometheus/node_exporter/releases)
``` bash
curl -SL https://github.com/prometheus/node_exporter/releases/download/v1.0.1/node_exporter-1.0.1.linux-armv7.tar.gz >ne.tgz
sudo tar xvzf ne.tgz -C /usr/local/bin/ --strip-components=1
rm ne.tgz

sudo mv tmpfile /etc/systemd/system/nodeexporter.service
sudo systemctl daemon-reload \
 && sudo systemctl enable nodeexporter \
 && sudo systemctl start nodeexporter
curl localhost:9100/metrics
```

## Config

## Alert Manager
Get latest version from alert manager [here](https://github.com/prometheus/alertmanager/releases)
``` bash
cd /opt
echo "curl -SL https://github.com/prometheus/alertmanager/releases/download/v0.21.0/alertmanager-0.21.0.linux-armv7.tar.gz >alert.tgz" | sudo sh
sudo tar xvzf alert.tgz
sudo rm alert.tgz
sudo ln -s alertmanager-0.21.0.linux-armv7 alertmanager

sudo mv /tmp/alertfile /etc/systemd/system/alertmanager.service
sudo systemctl daemon-reload \
 && sudo systemctl enable alertmanager \
 && sudo systemctl start alertmanager
```

## Links
https://www.mytinydc.com/en/blog/prometheus-grafana-installation/
https://blog.alexellis.io/prometheus-nodeexporter-rpi/
